﻿using System;
using System.Collections.Generic;

namespace Lab8_Ex3
{
    class Program
    {
        static void Main()
        {
            string[] array = new string[] { "code", "doce", "ecod", "framer", "frame" };
            List<string> newArray = DeleteTheAnagrams(array);
            newArray.Sort();

            string words = String.Join("', '", newArray);
            Console.Write("result = ['{0}']", words);
            Console.ReadKey();
        }

        static List<string> DeleteTheAnagrams(string[] array)
        {
            var arrWithoutAnagrams = new List<string>();
            var sortedByCharArray = new List<string>();

            foreach(var word in array)
            {
                char[] charArray = word.ToCharArray();
                Array.Sort(charArray);
                string value = string.Join("", charArray);

                if (!sortedByCharArray.Contains(value))
                {
                    arrWithoutAnagrams.Add(word);
                    sortedByCharArray.Add(value);
                }
            }

            return arrWithoutAnagrams;
        }
    }
}